package ru.volnenko.se.listener.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;

/**
 * @author Denis Volnenko
 */
@Component
public final class ProjectClearListener extends AbstractListener {

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        projectRepository.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

}
