package ru.volnenko.se.listener.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;

/**
 * @author Denis Volnenko
 */
@Component
public final class TaskClearListener extends AbstractListener {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    @EventListener(condition = "@taskClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        taskRepository.clear();
        System.out.println("[ALL TASK REMOVED]");
    }

}
