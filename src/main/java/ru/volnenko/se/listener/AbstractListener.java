package ru.volnenko.se.listener;

import ru.volnenko.se.controller.Bootstrap;
import ru.volnenko.se.event.ConsoleEvent;

/**
 * @author Denis Volnenko
 */
public abstract class AbstractListener {

    protected Bootstrap bootstrap;

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract void handler(final ConsoleEvent event) throws Exception;

    public abstract String command();

    public abstract String description();

}
