package ru.volnenko.se.controller;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;
import ru.volnenko.se.error.CommandCorruptException;

import java.util.*;

/**
 * @author Denis Volnenko
 */
@Component
public class Bootstrap implements InitializingBean {

    @Autowired
    private List<AbstractListener> commandList;

    @Autowired
    private ApplicationEventPublisher publisher;

    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();

    private final Scanner scanner = new Scanner(System.in);

    public void registry(List<AbstractListener> list) {
        for (AbstractListener command : list) {
            final String cliCommand = command.command();
            final String cliDescription = command.description();
            if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
            if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
            command.setBootstrap(this);
            commands.put(cliCommand, command);
        }
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            if(command == null || command.isEmpty()) return;
            publisher.publishEvent(new ConsoleEvent(command));
        }
    }

    public List<AbstractListener> getListCommand() {
        return new ArrayList<>(commands.values());
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        registry(commandList);
    }
}
